﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Day1
{
    class Program
    {
        static void Main(string[] args)
        {
            // int n = int.Parse(Console.ReadLine());
            // int[] x = Array.ConvertAll(Console.ReadLine().Split(' '), arTemp => Convert.ToInt32(arTemp));

            int n = 10;
            int[] x = new int[]{3, 7, 8, 5, 12, 14, 21, 15, 18, 14};
            // var xList = x.Distinct().ToList();

            // xList.Sort();
            Array.Sort(x);
            foreach (var item in x)
            {
                System.Console.Write(item + " ");
            }System.Console.WriteLine();

            int lowerBound = 0;
            int midRange = Median(x, n);
            int upperBound = 0;

            if(n % 2 == 0){
                lowerBound = Median(x.Take(x.Length / 2).ToArray(), x.Length / 2);
                upperBound = Median(x.Skip(x.Length / 2).Take(x.Length/2).ToArray(), x.Length / 2);
            }else{
                lowerBound = Median(x.Take(x.Length / 2).ToArray(), x.Length / 2);
                upperBound = Median(x.Skip(x.Length / 2 + 1).Take(x.Length / 2).ToArray(), x.Length / 2);
            }

            System.Console.WriteLine(lowerBound);
            System.Console.WriteLine(midRange);
            System.Console.WriteLine(upperBound);
            

            
        }

        static int Median(int[] arr, int arrSize)
        {
            if(arrSize % 2 == 0){
                return (arr[arrSize / 2 - 1] + arr[arrSize/2]) / 2;
            } else{
                return (arr[arrSize / 2]);
            }
        }
    }
}
